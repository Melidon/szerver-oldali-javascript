# Routes

## Home

- GET `/`
  - renderMV(index.html)

## Character

- GET `/character/create`
  - renderMV(character_create.html)

- POST `/character/create`
  - createCharacterMW()
    - On success: redirect to `/character/list`
    - On error: keep the form data

- GET `/character/list`
  - listCharactersMW()
  - renderMV(character_list.html)

- GET `/character/edit/:character_id`
  - getCharacterMW()
  - renderMV(character_edit.html)

- PUT `/character/edit/:character_id`
  - updateCharacterMW()
    - On success: redirect to `/character/list`
    - On error: keep the form data

- DELETE `/character/delete/:character_id`
  - deleteCharacterMW()

## Equipment

- GET `/equipment/create`
  - renderMV(equipment_create.html)

- POST `/equipment/create`
  - createEquipmentMW()
    - On success: redirect to `/equipment/list`
    - On error: keep the form data

- GET `/equipment/list`
  - listEquipmentsMW()
  - renderMV(equipment_list.html)

- GET `/equipment/edit/:equipment_id`
  - getEquipmentMW()
  - renderMV(equipment_edit.html)

- PUT `/equipment/edit/:equipment_id`
  - updateEquipmentMW()
    - On success: redirect to `/equipment/list`
    - On error: keep the form data

- DELETE `/equipment/delete/:equipment_id`
  - deleteEquipmentMW()
