/**
 * Render a view.
 */
function render(viewName) {
  return function (req, res) {
    res.render(viewName, res.locals);
  };
}

module.exports = render;
