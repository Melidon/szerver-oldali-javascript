function requireOption(objectRepository, propertyName) {
  if (objectRepository?.[propertyName]) {
    return objectRepository[propertyName];
  }
  throw new TypeError(`${propertyName} required`);
}

module.exports = requireOption;
