const requireOption = require("./requireOption");

function equipmentIdFromRequestParams(params) {
  const equipment_id = params["equipment_id"];
  if (typeof equipment_id !== "string") {
    return null;
  }
  if (equipment_id.length !== 24) {
    return null;
  }
  return equipment_id;
}

function equipmentFromRequestBody(body) {
  if (typeof body.name !== "string" || typeof body.cost !== "string" || typeof body.weight !== "string" || typeof body.description !== "string") {
    return null;
  }
  const equipment = {
    name: body.name,
    cost: body.cost,
    weight: body.weight,
    description: body.description,
  };
  return equipment;
}

/**
 * Get equipment data from request body.
 * Save equipment to MongoDB.
 * Redirect to /equipment/list.
 */
function createEquipment(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  return async function (req, res, next) {
    const equipment = equipmentFromRequestBody(req.body);
    if (equipment === null) {
      return next("Invalid equipment data in request body!");
    }
    try {
      await Equipment.create(equipment);
    } catch (err) {
      return next(err);
    }
    res.redirect("/equipment/list");
  };
}

/**
 * Get equipment id from request param.
 * Get equipment based on id from MongoDB.
 * Place equipment in response locals.
 */
function getEquipment(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  return async function (req, res, next) {
    const equipment_id = equipmentIdFromRequestParams(req.params);
    if (equipment_id === null) {
      return next("Invalid equipment id in request params!");
    }
    try {
      const equipment = await Equipment.findById(equipment_id).exec();
      res.locals.equipment = equipment;
    } catch (err) {
      return next(err);
    }
    next();
  };
}

/**
 * List all equipments from MongoDB.
 * Place equipments in response locals.
 */
function listEquipment(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  return async function (req, res, next) {
    try {
      const equipments = await Equipment.find({}).exec();
      res.locals.equipments = equipments;
    } catch (err) {
      return next(err);
    }
    next();
  };
}

function listEquipmentForCharacter(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  return async function (req, res, next) {
    const character = res.locals.character;
    try {
      const character_equipments = await Equipment.find({ _id: { $in: character.equipment_ids } }).exec();
      res.locals.character_equipments = character_equipments;
    } catch (err) {
      return next(err);
    }
    next();
  };
}

/**
 * Get equipment id from request param.
 * Get equipment data from request body.
 * Update equipment based on id with data in MongoDB.
 * Redirect to /equipment/list.
 */
function updateEquipment(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  return async function (req, res, next) {
    const equipment_id = equipmentIdFromRequestParams(req.params);
    if (equipment_id === null) {
      return next("Invalid equipment id in request params!");
    }
    const equipment = equipmentFromRequestBody(req.body);
    if (equipment === null) {
      return next("Invalid equipment data in request body!");
    }
    try {
      await Equipment.updateOne({ _id: equipment_id }, equipment).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect("/equipment/list");
  };
}

/**
 * Get equipment id from request param.
 * Delete equipment based on id from MongoDB.
 * Redirect to /equipment/list.
 */
function deleteEquipment(objectRepository) {
  const Equipment = requireOption(objectRepository, "Equipment");
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const equipment_id = equipmentIdFromRequestParams(req.params);
    if (equipment_id === null) {
      return next("Invalid equipment id in request params!");
    }
    try {
      await Equipment.deleteOne({ _id: equipment_id }).exec();
      await Character.updateMany({ equipment_ids: equipment_id }, { $pull: { equipment_ids: equipment_id } }).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect("/equipment/list");
  };
}

module.exports = {
  create: createEquipment,
  get: getEquipment,
  list: listEquipment,
  listForCharacter: listEquipmentForCharacter,
  update: updateEquipment,
  delete: deleteEquipment,
};
