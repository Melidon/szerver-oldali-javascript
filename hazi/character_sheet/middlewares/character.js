const requireOption = require("./requireOption");

function characterIdFromRequestParams(params) {
  const character_id = params["character_id"];
  if (typeof character_id !== "string") {
    return null;
  }
  if (character_id.length !== 24) {
    return null;
  }
  return character_id;
}

function characterFromRequestBody(body) {
  if (typeof body.name !== "string" || typeof body.race !== "string" || typeof body.class !== "string" || typeof body.level !== "string") {
    return null;
  }
  const character = {
    name: body.name,
    race: body.race,
    class: body.class,
    level: body.level,
  };
  return character;
}

function equipmentIdFromRequestBody(body) {
  const equipment_id = body.equipment_id;
  if (typeof equipment_id !== "string") {
    return null;
  }
  if (equipment_id.length !== 24) {
    return null;
  }
  return equipment_id;
}

function equipmentIdFromRequestParams(params) {
  const equipment_id = params["equipment_id"];
  if (typeof equipment_id !== "string") {
    return null;
  }
  if (equipment_id.length !== 24) {
    return null;
  }
  return equipment_id;
}

/**
 * Get character data from request body.
 * Save character to MongoDB.
 * Redirect to /character/list.
 */
function createCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const character = characterFromRequestBody(req.body);
    if (character === null) {
      return next("Invalid character data in request body!");
    }
    try {
      await Character.create(character);
    } catch (err) {
      return next(err);
    }
    res.redirect("/character/list");
  };
}

/**
 * Get character id from request param.
 * Get character based on id from MongoDB.
 * Place character in response locals.
 */
function getCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const character_id = characterIdFromRequestParams(req.params);
    if (character_id === null) {
      return next("Invalid character id in request params!");
    }
    try {
      const character = await Character.findById(character_id).exec();
      res.locals.character = character;
    } catch (err) {
      return next(err);
    }
    next();
  };
}

/**
 * List all characters from MongoDB.
 * Place characters in response locals.
 */
function listCharacters(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    try {
      const characters = await Character.find({}).exec();
      res.locals.characters = characters;
    } catch (err) {
      return next(err);
    }
    next();
  };
}

/**
 * Get character id from request param.
 * Get character data from request body.
 * Update character based on id with data in MongoDB.
 * Redirect to /character/list.
 */
function updateCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function updateCharacter(req, res, next) {
    const character_id = characterIdFromRequestParams(req.params);
    if (character_id === null) {
      return next("Invalid character id in request params!");
    }
    const character = characterFromRequestBody(req.body);
    if (character === null) {
      return next("Invalid character data in request body!");
    }
    try {
      await Character.updateOne({ _id: character_id }, character).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect("/character/list");
  };
}

/**
 * Get character id from request param.
 * Get equipment id from request body.
 * Add equipment to character based on id in MongoDB.
 * Redirect to /character/edit/:character_id.
 */
function addEquipmentForCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const character_id = characterIdFromRequestParams(req.params);
    if (character_id === null) {
      return next("Invalid character id in request params!");
    }
    const equipment_id = equipmentIdFromRequestBody(req.body);
    if (equipment_id === null) {
      return next("Invalid equipment id in request body!");
    }
    try {
      await Character.updateOne({ _id: character_id }, { $addToSet: { equipment_ids: equipment_id } }).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect(`/character/edit/${character_id}`);
  };
}

function removeEquipmentForCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const character_id = characterIdFromRequestParams(req.params);
    if (character_id === null) {
      return next("Invalid character id in request params!");
    }
    const equipment_id = equipmentIdFromRequestParams(req.params);
    if (equipment_id === null) {
      return next("Invalid equipment id in request params!");
    }
    try {
      await Character.updateOne({ _id: character_id }, { $pull: { equipment_ids: equipment_id } }).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect(`/character/edit/${character_id}`);
  };
}

/**
 * Get character id from request param.
 * Delete character based on id from MongoDB.
 * Redirect to /character/list.
 */
function deleteCharacter(objectRepository) {
  const Character = requireOption(objectRepository, "Character");
  return async function (req, res, next) {
    const character_id = characterIdFromRequestParams(req.params);
    if (character_id === null) {
      return next("Invalid character id in request params!");
    }
    try {
      await Character.deleteOne({ _id: character_id }).exec();
    } catch (err) {
      return next(err);
    }
    res.redirect("/character/list");
  };
}

module.exports = {
  create: createCharacter,
  get: getCharacter,
  list: listCharacters,
  update: updateCharacter,
  addEquipment: addEquipmentForCharacter,
  removeEquipment: removeEquipmentForCharacter,
  delete: deleteCharacter,
};
