const expect = require("chai").expect;
const characterMW = require("../../../middlewares/character");

describe("create character middleware", function () {
  it("should call next with error on invalid character data", function (done) {
    const createCharacter = characterMW.create({
      Character: {},
    });
    const req = {
      body: {},
    };
    const res = undefined;
    const next = function (err) {
      expect(err).to.eql("Invalid character data in request body!");
      done();
    };
    createCharacter(req, res, next);
  });

  it("should call next with error on database error", function (done) {
    const createCharacter = characterMW.create({
      Character: {
        create: async function (character) {
          expect(character).to.eql({
            name: "John Doe",
            race: "Human",
            class: "Warrior",
            level: "Level 1",
          });
          throw new Error("Database error!");
        },
      },
    });
    const req = {
      body: {
        name: "John Doe",
        race: "Human",
        class: "Warrior",
        level: "Level 1",
      },
    };
    const res = undefined;
    const next = function (err) {
      expect(err).to.eql(new Error("Database error!"));
      done();
    };
    createCharacter(req, res, next);
  });

  it("should create a character from req.body and redirect", function (done) {
    const createCharacter = characterMW.create({
      Character: {
        create: async function (character) {
          expect(character).to.eql({
            name: "John Doe",
            race: "Human",
            class: "Warrior",
            level: "Level 1",
          });
        },
      },
    });
    const req = {
      body: {
        name: "John Doe",
        race: "Human",
        class: "Warrior",
        level: "Level 1",
      },
    };
    const res = {
      redirect: function (url) {
        expect(url).to.eql("/character/list");
        done();
      },
    };
    const next = undefined;
    createCharacter(req, res, next);
  });
});

describe("get character middleware", function () {
  it("should call next with error on invalid character id", function (done) {
    const getCharacter = characterMW.get({
      Character: {},
    });
    const req = {
      params: {
        character_id: "invalid",
      },
    };
    const res = undefined;
    const next = function (err) {
      expect(err).to.eql("Invalid character id in request params!");
      done();
    };
    getCharacter(req, res, next);
  });

  it("should call next with error on database error", function (done) {
    const getCharacter = characterMW.get({
      Character: {
        findById: function (id) {
          return {
            exec: async function () {
              expect(id).to.eql("5f7e1a0f9b1bde0e1c3a8a2b");
              throw new Error("Database error!");
            },
          };
        },
      },
    });
    const req = {
      params: {
        character_id: "5f7e1a0f9b1bde0e1c3a8a2b",
      },
    };
    const res = undefined;
    const next = function (err) {
      expect(err).to.eql(new Error("Database error!"));
      done();
    };
    getCharacter(req, res, next);
  });

  it("should place character on res.locals and call next", function (done) {
    const getCharacter = characterMW.get({
      Character: {
        findById: function (id) {
          return {
            exec: async function () {
              expect(id).to.eql("5f7e1a0f9b1bde0e1c3a8a2b");
              return {
                name: "John Doe",
                race: "Human",
                class: "Warrior",
                level: "Level 1",
              };
            },
          };
        },
      },
    });
    const req = {
      params: {
        character_id: "5f7e1a0f9b1bde0e1c3a8a2b",
      },
    };
    const res = {
      locals: {},
    };
    const next = function (err) {
      expect(res.locals.character).to.eql({
        name: "John Doe",
        race: "Human",
        class: "Warrior",
        level: "Level 1",
      });
      expect(err).to.eql(undefined);
      done();
    };
    getCharacter(req, res, next);
  });
});
