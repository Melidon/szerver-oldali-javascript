const equipmentMW = require("../middlewares/equipment");
const renderMW = require("../middlewares/render");

function registerHandlers(app) {
  const objectRepository = {
    Character: require("../models/character"),
    Equipment: require("../models/equipment"),
  };

  // Create
  app.get("/equipment/create", renderMW("equipment_create"));
  app.post("/equipment/create", equipmentMW.create(objectRepository));

  // List
  app.get("/equipment/list", equipmentMW.list(objectRepository), renderMW("equipment_list"));

  // Edit
  app.get("/equipment/edit/:equipment_id", equipmentMW.get(objectRepository), renderMW("equipment_edit"));
  app.post("/equipment/edit/:equipment_id", equipmentMW.update(objectRepository));

  // Delete
  app.post("/equipment/delete/:equipment_id", equipmentMW.delete(objectRepository));
}

module.exports = registerHandlers;
