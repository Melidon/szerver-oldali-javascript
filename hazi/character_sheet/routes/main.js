const registerCharacterHandlers = require("./character");
const registerEquipmentHandlers = require("./equipment");

function registerHandlers(app) {
  registerCharacterHandlers(app);
  registerEquipmentHandlers(app);
  app.get("/", (req, res) => {
    res.render("index");
  });
}

module.exports = registerHandlers;
