const characterMW = require("../middlewares/character");
const equipmentMW = require("../middlewares/equipment");
const renderMW = require("../middlewares/render");

function registerHandlers(app) {
  const objectRepository = {
    Character: require("../models/character"),
    Equipment: require("../models/equipment"),
  };

  // Create
  app.get("/character/create", equipmentMW.list(objectRepository), renderMW("character_create"));
  app.post("/character/create", characterMW.create(objectRepository));

  // List
  app.get("/character/list", characterMW.list(objectRepository), renderMW("character_list"));

  // Edit
  app.get(
    "/character/edit/:character_id",
    characterMW.get(objectRepository),
    equipmentMW.list(objectRepository),
    equipmentMW.listForCharacter(objectRepository),
    renderMW("character_edit")
  );
  app.post("/character/edit/:character_id", characterMW.update(objectRepository));
  app.post("/character/edit/:character_id/equipment/add", characterMW.addEquipment(objectRepository));
  app.post("/character/edit/:character_id/equipment/remove/:equipment_id", characterMW.removeEquipment(objectRepository));

  // Delete
  app.post("/character/delete/:character_id", characterMW.delete(objectRepository));
}

module.exports = registerHandlers;
