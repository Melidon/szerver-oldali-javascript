const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.set("view engine", "ejs");

app.use(bodyParser.urlencoded());
// app.use(express.json());

const registerHandlers = require("./routes/main");
registerHandlers(app);

const port = 3000;

const server = app.listen(port, () => {
  console.log(`Development Server is listening on localhost:${port}, open your browser on http://localhost:${port}/`);
});
