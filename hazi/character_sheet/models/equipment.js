const db = require("../config/db");

const Equipment = db.model("Equipment", {
  name: String,
  cost: String,
  weight: String,
  description: String,
});

module.exports = Equipment;
