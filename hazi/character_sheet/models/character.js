const Schema = require("mongoose").Schema;
const db = require("../config/db");

const Character = db.model("Character", {
  name: String,
  race: String,
  class: String,
  level: String,
  equipment_ids: [{ type: Schema.Types.ObjectId, ref: "Equipment" }],
});

module.exports = Character;
