# Karakterlap

## Apalötlet

Az webalkalmazásban azt szeretném, hogy létre lehessen hozni (illetve módosítani és törölni) különböző tárgyakat mit példálul fegyverek, páncélok, varázstárgyak.

Emellett szeretném, hogy lehessen használni karakterek adminisztrálására is. Ezek a karakterek természetesen rendelkezhetnek a fentebb említett tárgyakkal.

## Entitások

Minden tárgy rendelkezik a következő attribútumokkal: név, költség, súly, leírás.

A karakterek a következőkkel rendelkeznek: név, osztály, szint, faj és felszrelés.

## Funkciók

Alapból a kezdőoldalon leszünk, itt egy rövid leírás lesz csak, hogy mi is ez az oldal. Illetve navbar segíségével át lehet menni a tárgyak és a karakterek oldalra.

A tárgyak oldal listában mutatja az összes tárgyat. Itt lehet új tárgyat létrehozni. A listás nézetben nem látszik a tárgy leírása, de ha rákattintunk a tárgyra akkor a részletek oldalon megjelenik a tárgy leírása is. Ezen az oldalon lehet módosítani és törölni is a tárgyat.

A karakterek oldal listában mutatja az összes karaktert. Itt lehet egy gombbal új karaktert létrehozni. A listás nézetben nem látszik a karakter felszerelése, de ha rákattintunk a karakterre akkor egy új oldalon megjelenik a karakter felszerelése is. A felszerelés az adatbázisban lévő tárgyakból áll össze, ezt szabadon lehet módosítani, csak úgy mint a karakter szintjét. A név, osztály és faj azonban nem módosítható. Ha valakinek nem tetszik a karakter, akkor törölheti és létrehozhat egy olyat ami már tetszik neki.

Fontos szélsőséges esetek. Amennyiben egy karaktert törölnek, akkor a nála lévő tárgyakkal nem történik semmi sem, ugyanúgy benne maradnak az adatbázisban. Viszont ha egy tárgyat törölnek, akkor az összes karakter elveszíti azt.

A létrehozás képernyő ugyanaz mint a részletek képernyő, mind a karakterek, mint a felszerelések esetében.